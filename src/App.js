import React from 'react';
import logo from './logo.svg';
import { Counter } from './features/counter/Counter';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Counter />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <span style={{ fontSize: "16px" }}>
          <span>Về tác giả Hỏi Dân IT: </span>
          <a
            className="App-link"
            href="https://hoidanit.com.vn/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Website
          </a>

          ,<span>  </span>
          <a
            className="App-link"
            href="https://youtube.com/@hoidanit/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Youtube
          </a>
        </span>
      </header>
    </div>
  );
}

export default App;
